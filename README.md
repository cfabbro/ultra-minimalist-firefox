# ultra-minimalist-firefox

Ultra-minimalist "one line" Firefox using UserChrome.css and Tree Tabs extension

Note: **Firefox 65.0 broke something fundamental**  
- `#titlebar {display: none}` no longer works
- Strange behavior on various sites (like YouTube) where mouse is registering in wrong location
- Tree Tabs is getting stuck in a strange startup loop even without any UserChrome.css  
- etc..

So for now, switching to Tree Style Tabs and disabling this UserChrome.css seems the only option :(
I will try to fix it once I get some time...